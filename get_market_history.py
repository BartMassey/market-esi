# Copyright © 2017 Bart Massey
# [This program is licensed under the "MIT License"]
# Please see the file COPYING in the source
# distribution of this software for license terms.

# Get some market data using ESI.

from __future__ import print_function
import time
import esi
from esi.rest import ApiException
from pprint import pprint

api = esi.LiveApi()
regions = [("Rens", 10000030), ("Hek", 10000042)]
types = [("Wetware Mainframe", 2876),
         ("Biotech Research Reports", 2358),
         ("Supercomputers", 2349),
         ("Cryoprotectant Solution", 2367)]
for region_name, region_id in regions:
    for type_name, type_id in types:
        try: 
            # List historical market statistics in region
            api_response = \
              api.get_markets_region_id_history(region_id, type_id)
            for market_day in api_response:
                print(region_name, type_name,
                      market_day.date,
                      market_day.lowest,
                      market_day.average,
                      market_day.highest,
                      market_day.volume,
                      sep=",")
        except ApiException as e:
            print("Exception when calling",
                  "LiveApi->get_markets_region_id_history: %s\n" % e)
