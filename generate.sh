#!/bin/sh
# Copyright © 2017 Bart Massey
# [This program is licensed under the "MIT License"]
# Please see the file COPYING in the source
# distribution of this software for license terms.

# Generate ESI Python binding using `swagger-codegen`.

exec swagger-codegen generate \
     -i 'https://esi.tech.ccp.is/latest/swagger.json?datasource=tranquility' \
     -l python \
     --invoker-package esi \
     --api-package esi \
     --model-package esi \
     -DpackageName=esi \
     -o esi-client-python
