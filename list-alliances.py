# Copyright © 2017 Bart Massey
# [This program is licensed under the "MIT License"]
# Please see the file COPYING in the source
# distribution of this software for license terms.

# Example code: list all alliances using ESI.

from __future__ import print_function
import time
import esi
from esi.rest import ApiException
from pprint import pprint
# create an instance of the API class
api_instance = esi.AllianceApi()
datasource = 'tranquility'

try:
    # List all alliances
    api_response = api_instance.get_alliances(datasource=datasource)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AllianceApi->get_alliances: %s\n" % e)
