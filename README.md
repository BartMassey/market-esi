# market-esi
Copyright (c) 2017 Bart Massey

`market-esi` is a bunch of toy test code for playing
with EvE Online's Eve Swagger Interface (ESI) API for market
data.

This work is licensed under the "MIT License". Please see
the file COPYING in this distribution for license details.
